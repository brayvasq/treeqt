import sys
from PyQt5.Qt import QGraphicsView, QGraphicsScene, QFont, QPointF
from PyQt5.QtGui import QPen, QColor
from models.node import Node


class Panel(QGraphicsView):
    def __init__(self):
        super(Panel, self).__init__()

        self.tree = None
        self.y = 50
        self.scene = QGraphicsScene()

        self.setScene(self.scene)

    def draw(self):
        self.scene.clear()
        self.paint()

    def paint(self):
        root = self.tree.get_root()
        self.y = 50
        self.__paint_tree(root, 10, self.y)
        self.__lines(root)

    def __paint_tree(self, root: Node, x, y):
        if (root != None):
            pen = QPen(QColor(0, 0, 0))
            pen2 = QPen(QColor(50, 200, 50))

            root.x = x
            root.y = self.y

            xp = root.tam / 2

            self.scene.addEllipse(root.x, root.y, root.tam, root.tam, pen)

            f = QFont()
            f.setPixelSize(10)
            f.setBold(False)
            item = self.scene.addText(str(root.data), f)
            item.setPos(QPointF(root.x + 3, root.y - 2))
            x += 50

            if (root.left != None):
                self.y += 40
                self.__paint_tree(root.left, x, self.y)
            if (root.right != None):
                self.y += 40
                self.__paint_tree(root.right, x, self.y)

    def __lines(self, root: Node):
        if (root != None):
            pen = QPen(QColor(0, 0, 0))
            xp = root.tam / 2

            if (root.left != None):
                xl1_a = root.x + root.tam + 5
                yl1_a = root.y + root.tam - 5

                ylb = root.left.y + xp
                self.scene.addLine(root.x + root.tam, yl1_a, xl1_a, yl1_a)
                self.scene.addLine(xl1_a,yl1_a,xl1_a,ylb,pen)
                self.scene.addLine(xl1_a,ylb,root.left.x,ylb,pen)
                self.__lines(root.left)

            if (root.right != None):
                xl1_a = root.x + root.tam + 5
                yl1_a = root.y + root.tam - 5

                ylb = root.right.y + xp
                self.scene.addLine(root.x + root.tam, yl1_a, xl1_a, yl1_a)
                self.scene.addLine(xl1_a,yl1_a,xl1_a,ylb,pen)
                self.scene.addLine(xl1_a,ylb,root.right.x,ylb,pen)
                self.__lines(root.right)