from PyQt5.Qt import QMainWindow
from PyQt5 import uic
from views.widget import Widget
from models.tree import Tree
import os
class Window(QMainWindow):

    def __init__(self):
        QMainWindow.__init__(self)

        ui_path = os.path.dirname(os.path.abspath(__file__))
        uic.loadUi(os.path.join(ui_path, "window.ui"))
        #uic.loadUi("window.ui",self)
        self.setWindowTitle("Binary Tree")

        self.principal = Widget()
        self.setCentralWidget(self.principal)

        self.tree = Tree()
        self.tree.add(4)
        self.tree.add(2)
        self.tree.add(3)
        self.tree.add(5)
        self.principal.tree_view.tree = self.tree

        self.principal.tree_view.draw()

