from PyQt5.Qt import QWidget,QLineEdit,QPushButton,QVBoxLayout,QHBoxLayout
from PyQt5.QtWidgets import QMessageBox
from views.tree_view import Panel

class Widget(QWidget):

    def __init__(self,parent=None):
        QWidget.__init__(self)

        self.tree_view = Panel()
        self.input = QLineEdit()
        self.button = QPushButton("Add")

        layoutV = QVBoxLayout()

        layoutH = QHBoxLayout()

        layoutH.addWidget(self.input)
        layoutH.addWidget(self.button)

        layoutV.addLayout(layoutH)
        layoutV.addWidget(self.tree_view)
        self.setLayout(layoutV)

        self.__signals()

    def __signals(self):
        self.button.clicked.connect(self.__add_item)

    def __add_item(self):
        try:
            data = int(self.input.text())
            self.tree_view.tree.add(data)
            self.tree_view.draw()
        except:
            print("An error occurs adding a new item")
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)

            msg.setText("An error occurs adding a new item")
            msg.exec_()
