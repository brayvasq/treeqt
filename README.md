# binary_tree_qt
Graphical binary tree in PyQt5

## Virtualenv
```bash
# Creating environment
virtualenv .
# Installing dependencies
pip install -r requirements.txt
# Running app
python main.py
```

## Docker
### Build image

```bash
docker build -t fadawar/pyqt5 .
```

### Run image

```bash
docker run -it \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v $(pwd):/app \
    -e DISPLAY=$DISPLAY \
    -u qtuser \
    fadawar/docker-pyqt5 python3 /app/main.py
```

#### Preview

![Image Preview](https://bytebucket.org/brayvasq/binary_tree_qt/raw/2a93e62d8791870fd0cfaf823d99097debc7caf8/images/binary_tree.PNG)

### Credits 

https://github.com/jozo/docker-pyqt5

https://stackoverflow.com/questions/49478064/error-loading-pyqt5-ui-file