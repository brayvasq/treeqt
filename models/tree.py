from models.node import Node

"""
    Class Tree
    @author brayvasq
"""
class Tree():

    def __init__(self):
        self.__root:Node = None

    def get_root(self):
        return self.__root
    """
        add method -> need a val and add val to the tree
    """
    def add(self,val:int):
        if(self.__root == None):
            self.__root = Node(val)
        else:
            parent = Node()
            aux = self.__root

            while(aux != None):
                parent = aux
                if(val>aux.data):
                    aux = aux.right
                elif(val<aux.data):
                    aux = aux.left

            if(val>parent.data):
                parent.right = Node(val)
            elif(val < parent.data):
                parent.left = Node(val)


    """
        public preorder path
    """
    def pre_order(self):
        return self.__pre_order(self.__root)
    """
        private preorder path
        need a root for do recursive path
    """
    def __pre_order(self,root:Node):
        if(root != None):
            print(root.data)
            self.__pre_order(root.left)
            self.__pre_order(root.right)

    """
        public postorder path
    """

    def post_orden(self):
        return self.__post_order(self.__root)

    """
        private postorder path
        need a root for do recursive path
    """

    def __post_order(self, root: Node):
        if (root != None):
            self.__post_order(root.left)
            self.__post_order(root.right)
            print(root.data)

    """
        public postorder path
    """

    def in_orden(self):
        return self.__in_order(self.__root)

    """
        private inorder path
        need a root for do recursive path
    """

    def __in_order(self, root: Node):
        if (root != None):
            self.__in_order(root.left)
            print(root.data)
            self.__in_order(root.right)

    def print_tree(self):
        self.__print_tree(" ",True,self.__root)

    def __print_tree(self, prefix, isTail,root:Node):

        if (root != None):
            x = "└── " if isTail else "├── "

            print(prefix + x + str(root.data))

            x = "    " if isTail else "│   "

            if(root.left!=None):
                self.__print_tree(prefix+x,True,root.left)
            if(root.right!=Node):
                self.__print_tree(prefix+x,True,root.right)


